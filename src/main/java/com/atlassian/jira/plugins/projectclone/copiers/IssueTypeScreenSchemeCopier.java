/*
 * Copyright © 2013 - 2015 Atlassian Pty Ltd. Licensed under the Apache License, Version
 *  2.0 (the "License"); you may not use this file except in compliance with the License. You
 *  may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless
 *  required by applicable law or agreed to in writing, software distributed under the License is
 *  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 *  either express or implied. See the License for the specific language governing permissions
 *  and limitations under the License.
 */

package com.atlassian.jira.plugins.projectclone.copiers;

import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenScheme;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenSchemeManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.util.ErrorCollection;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * TODO: Document this class / interface here
 *
 * @since v6.0
 */
public class IssueTypeScreenSchemeCopier extends PermissionSchemeCopier {
	@Autowired
	protected IssueTypeScreenSchemeManager issueTypeScreenSchemeManager;

    @Autowired
    protected FieldConfigSchemeManager configSchemeManager;

    @Autowired
    protected FieldManager fieldManager;

	@Override
	public void copySettings(Project project, Project newProject, ErrorCollection errorCollection) {
        final IssueTypeScreenScheme fieldConfigScheme = issueTypeScreenSchemeManager.getIssueTypeScreenScheme(project);
        if (fieldConfigScheme != null) {
            final IssueTypeScreenScheme newConfigScheme = issueTypeScreenSchemeManager.getIssueTypeScreenScheme(newProject);
            if (fieldConfigScheme.equals(newConfigScheme)) {
                return;
            }

            issueTypeScreenSchemeManager.removeSchemeAssociation(newProject, newConfigScheme);
            issueTypeScreenSchemeManager.addSchemeAssociation(newProject, fieldConfigScheme);
        }
	}
}
