/*
 * Copyright © 2013 - 2015 Atlassian Pty Ltd. Licensed under the Apache License, Version
 *  2.0 (the "License"); you may not use this file except in compliance with the License. You
 *  may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless
 *  required by applicable law or agreed to in writing, software distributed under the License is
 *  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 *  either express or implied. See the License for the specific language governing permissions
 *  and limitations under the License.
 */

package com.atlassian.jira.plugins.projectclone.copiers;

import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.plugins.projectclone.extensions.ProjectSettingsCopier;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.util.ErrorCollection;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * TODO: Document this class / interface here
 *
 * @since v6.0
 */
public class VersionsCopier implements ProjectSettingsCopier {
	@Autowired
	protected VersionManager versionManager;

	@Override
	public void copySettings(Project project, Project newProject, ErrorCollection errorCollection) {
		List<Version> versions = versionManager.getVersions(project);
		for(Version version : versions) {
			// Version createVersion(final String name, final Date startDate, final Date releaseDate, final String description, final Long projectId, final Long scheduleAfterVersion) throws CreateException;
			try {
				// TODO: doesn't handle versions ordering
				versionManager.createVersion(version.getName(), version.getStartDate(), version.getReleaseDate(),
						version.getDescription(), newProject.getId(), null);
			} catch (CreateException e) {
				throw new RuntimeException(e);
			}

		}
	}
}
